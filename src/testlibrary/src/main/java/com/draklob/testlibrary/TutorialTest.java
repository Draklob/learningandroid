package com.draklob.testlibrary;

import android.util.Log;

import com.unity3d.player.UnityPlayer;


public class TutorialTest {
    public String GetTextFromPlugin( int number )
    {
        return "Number is " + number;
    }

    public void ShowingMessageFromPlugin()
    {
        Log.d( "Unity", "Native logcat message");
    }

    public void LogNumberSentFromUnity( int number )
    {
        Log.d("Unity", "Number passes is: " + number);
    }

    public int AddFiveToMyNumber( int number )
    {
        number += 5;
        return number;
    }

    public void CallAorB( String value )
    {
        Log.d("Unity", value);
        if( value.equals("A"))
        {
            DoSomethingA();
        } else if(value.equals("B")) {
            DoSomethingB();
        }
    }

    private void DoSomethingA()
    {
        UnityPlayer.UnitySendMessage("PluginScript", "ChangeTextToA", "1");
    }

    private void DoSomethingB()
    {
        UnityPlayer.UnitySendMessage("PluginScript", "ChangeTextToB", "2");
    }

}
